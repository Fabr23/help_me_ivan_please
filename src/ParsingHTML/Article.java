package ParsingHTML;

public class Article {
    private String name;

    public Article(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Название статьи: "+ name;
    }
}
