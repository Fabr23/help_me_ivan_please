package MyShitCipher;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

public class MyShitCipher {
    static void fileProcessor(int cipherMode, String key, File inputFile, File outputFile) {
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            inputBytes[100] = 11;
            inputBytes[101] = 12;

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | IOException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String key = "This is a secret";
        File inputFile = new File("E:\\ForBitBucket\\src\\MyShitCipher\\this.png");
        File encryptedFile = new File("E:\\ForBitBucket\\src\\MyShitCipher\\this.encrypted");
        File decryptedFile = new File("E:\\ForBitBucket\\src\\MyShitCipher\\decrypted-this.png");

        decryptedFile.canWrite();
        try {
            MyShitCipher.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);
        } catch (Exception e) {
        MyShitCipher.fileProcessor(Cipher.DECRYPT_MODE,key,encryptedFile,decryptedFile);
        System.out.println(e.getMessage());
        e.printStackTrace();
        }
    }
}
