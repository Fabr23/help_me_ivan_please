package ParsingHTML;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class ParsingHTML {
    public static void main(String[] args) throws IOException {
        ArrayList<Article> articleList = new ArrayList<>();

        Document doc = Jsoup.connect("http://yaplakal.com").get();

        Elements h2Elements = doc.getElementsByAttributeValue("class", "mainpage");
        h2Elements.forEach(h2Element -> {
            Element aElement = h2Element.child(0);
            articleList.add(new Article(aElement.text()));
        });
        articleList.forEach(System.out::println);
    }
}
